// Assignment 4
// - A database-esque thingy.
// Author: Chad Dulake
// Version: 1.01 (r48)
//
// ############################
// #  EXECUTION INSTRUCTIONS  #
// ############################
// 1. Position the guillotine over the executable
// 2. oh... wrong execution.
// - Execute the program with no arguments
// - Program will provide an interactive shell
// - Type HELP for a listing of commands & usage
// - All shell commands & names of People are case-insensitive

#if defined(__unix__)
#include <unistd.h>
#else
#define true 1
#define false 0
#include <stdio.h>
#include <stdlib.h>
#include <io.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define DIE_IF(COND, MSG) if(COND){fprintf(stderr, MSG); return 1;}
#define commandListSize 7
#define person_nameSize 28

typedef struct Person Person;

struct Person
{
	unsigned int id;
	char name[person_nameSize];
};

const char databaseFilePath[] = "./dulake.bin";
int databaseFileDescriptor;
const Person nullPerson = {0};
const char commandPrompt[] = "\n$>";
const char* commandList[commandListSize] = {"HELP", "ADD", "GET", "REMOVE", "PRINTDB", "EXIT", "XYZZY"};
const char* commandUsage[commandListSize] = {"HELP\n - This help message\n",
							   "\nADD <id> <name>\n - Adds a person to the database, ID MUST be a positive number\n",
							   "\nGET <name>\n - Finds a person by name in the database\n",
							   "\nREMOVE <name>\n - Removes the person from the database\n",
							   "\nPRINTDB\n - Prints the entire database to stdout\n",
							   "\nEXIT\n - Ends the interactive shell and closes the database program",
							   ""};

void initShell();
int getCommandID(char cmd[]);
void processCommand(int cmdID, int argc, char** argv);
void printHelp();
void add(Person* person);
void writeLine(int line, Person* person);
Person* readLine(int line);
int get(char* name);
void removePerson(char* name);
void printPerson(Person* person);
void printDB();

/* main()
 */
int main(int argc, char* argv[])
{
	databaseFileDescriptor = open(databaseFilePath, O_RDWR | O_BINARY | O_CREAT | O_TRUNC, S_IWRITE | S_IREAD);
	DIE_IF(databaseFileDescriptor == -1, "Error opening/creating database file for reading & writing")

	initShell();

	close(databaseFileDescriptor);
	return 0;
}

/* initShell()
 * instantiates an interactive shell and awaits user input, continues to run until user sends EXIT command.
 * dispatches commands to processCommand() (except EXIT command)
 */
void initShell()
{
	int argumentCount = 0;
	int const argumentBufferCount = 4;
	int const argumentBufferSize = 64;
	char argumentBufferBlock[256] = {0};
	char* argumentBuffers[4] = {&argumentBufferBlock[0], &argumentBufferBlock[64], &argumentBufferBlock[128], &argumentBufferBlock[192]}; // :D
	while(true)
	{
		fprintf(stdout, "%s ", commandPrompt);
		for(int i = 0; i < argumentBufferCount; i++)
		{
			argumentCount = i;
			for(int j = 0; j < argumentBufferSize - 1; j++)
			{
				int key = fgetc(stdin);
				if(key == ' ') // End of argument input, switch to next buffer.
				{
					argumentBuffers[i][j] = 0;
					break;
				}
				if(key == '\n') // End of line, no more arguments may exist, end buffer and end parsing.
				{
					argumentBuffers[i][j] = 0;
					i = argumentBufferSize;
					break;
				}
				argumentBuffers[i][j] = key;
			}
		}

		int cmdID = getCommandID(argumentBuffers[0]);
		if(cmdID == 5) // EXIT command, terminate shell.
			return;

		processCommand(cmdID, argumentCount, &argumentBuffers[1]); // Pass the 2nd buffer so the command called is not an argument.
	}
}

/* getCommandID()
 * returns the corresponding command ID for specified command string
 * returns -1 if command is unknown
 */
int getCommandID(char cmd[])
{
	cmd = strupr(cmd);
	for(int i = commandListSize - 1; i >= 0; i--)
	{
		if(strcmp(cmd, commandList[i]) == 0)
			return i;
	}
	return -1;
}

/* processCommand()
 * Runs the specified command, parses & passes the arguments to the command.
 * Executes HELP command if command is passed incorrect number of arguments or the command is unknown.
 */
void processCommand(int cmdID, int argc, char** argv)
{ 
	int id;
	Person person = {0};
	switch(cmdID)
	{
		case 0: // HELP
			printHelp();
			return;

		case 1: // ADD
			if(argc != 2)
				break;

			id = atoi(argv[0]);
			if(id < 0)
				break;
			person.id = id;

			int size;
			if(strlen(argv[1]) > person_nameSize)
				size = person_nameSize;
			else
				size = strlen(argv[1]);
			memcpy(person.name, argv[1], size);

			add(&person);
			return;

		case 2: // GET
			if(argc != 1)
				break;

			id = get(argv[0]);
			if(id < 0)
				fprintf(stdout, "Could not find: %s\n", argv[0]);
			else
				fprintf(stdout, "Found ID: %u\n", id);
			return;

		case 3: // REM
			if(argc != 1)
				break;

			removePerson(argv[0]);
			return;

		case 4: // OUT
			if(argc != 0)
				break;

			printDB();
			return;

		case 6: // XYZZY
			fprintf(stdout, "Nothing happens.\n");
			return;
	}
	printHelp();
}

/* printHelp()
 * Prints command usage list to stdout.
 */
void printHelp()
{
	fprintf(stdout, "\n");
	for(int i = 0; i < commandListSize; i++)
		fprintf(stdout, "%s", commandUsage[i]);
	fprintf(stdout, "\n");
}

/* add(Person)
 * Inserts a person into the database
 * The person's name & number should be printed to stdout letting the user know they have been added successfully.
 */
void add(Person* person)
{
	writeLine(-1, person); // Append the new person to the end of the file
	printPerson(person);
}

/* writeLine()
 * writes the specified line to the file
 * Line numbers start at 0
 * Line number of -1 means append, -2 means overwrite last line.
 * Line number of 0 means write first line, 1 means write second line.
 */
void writeLine(int line, Person* person)
{
	int seekMode = line >= 0 ? SEEK_SET : SEEK_END;
	if(line < 0)
		line++;
	if(lseek(databaseFileDescriptor, line * sizeof(Person), seekMode) < 0)
		return; // Error

	if(write(databaseFileDescriptor, person, sizeof(Person)) < 0)
		return; // Error
}

/* readLine()
 * reads the specified line from the file
 * returns NULL if past end of file or out of bounds.
 * returns the Person read from specified line
 * Assumes the database being read was written by the same version of this program (has the same sizes)
 */
Person* readLine(int line)
{
	Person* result = (Person*) malloc(sizeof(Person));
	int readBytes;
	if(lseek(databaseFileDescriptor, line * sizeof(Person), SEEK_SET) < 0)
		return NULL; // EOF/Out of bounds
	readBytes = read(databaseFileDescriptor, result, sizeof(Person));
	if(readBytes == 0)
		return NULL; // EOF
	if(readBytes != sizeof(Person))
		fprintf(stderr, "Warning # of bytes read does not match the size of a Person\n");

	return result;
}

/* get(char* name)
 * Retrieves the person's id number.
 * Main function should print the number returned to STDOUT.
 * Returns -1 if no result.
 */
int get(char* name)
{
	Person* personBuffer;
	int readBytes;
	for(int i = 0; true; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break;

		if(stricmp(personBuffer->name, name) == 0)
		{
			int result = personBuffer->id;
			free(personBuffer);
			return result;
		}

		free(personBuffer);
	}
	return -1;
}

/* removePerson(char* name)
 * name must be a C String.
 * name is case-insensitive.
 * Removes the person from the database & compacts the gap where the record was.
 * The person's name & id number is printed to stdout if removal was successful.
 */
void removePerson(char* name)
{
	int readBytes;
	Person* personBuffer;
	for(int i = 0; true; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break; // EOF
		if(stricmp(personBuffer->name, name) != 0)
		{
			free(personBuffer);
			continue;
		}
		printPerson(personBuffer);
		free(personBuffer);

		for(int j = 0; true; j++)
		{
			personBuffer = readLine(i + j + 1);
			if(personBuffer == NULL)
			{
#if !defined(__unix__)
				_chsize(databaseFileDescriptor, lseek(databaseFileDescriptor, -sizeof(Person), SEEK_END)); // Darn you Microsoft.
#else
				ftruncate(databaseFileDescriptor, lseek(databaseFileDescriptor, -sizeof(Person), SEEK_END)); // POSIX
#endif
				return; // EOF
			}
			writeLine(i + j, personBuffer);
			free(personBuffer);
		}
	}
}

/* printPerson()
 * Prints the person's information to stdout
 * Assures proper handling if the name is not a null terminated string
 */
void printPerson(Person* person)
{
	fprintf(stdout, "ID: %u, NAME: ", person->id);
	fwrite(person->name, sizeof(char), person_nameSize, stdout);
	fprintf(stdout, "\n");
}

/* printDB()
 * prints the name & id numbers of everyone in the database
 */
void printDB()
{
	Person* personBuffer;
	for(int i = 0; true; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break;

		printPerson(personBuffer);
		free(personBuffer);
	}
}